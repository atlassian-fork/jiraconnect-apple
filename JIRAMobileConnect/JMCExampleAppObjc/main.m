//
//  main.m
//  JMCExampleAppObjc
//
//  Created by Rene Cacheaux on 11/7/15.
//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
