//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit
#if Carthage
import WebImage
#else
import SDWebImage
#endif

class ComposeFeedbackView: UIView {
  @IBOutlet weak var feedbackTextView: UITextView!
  @IBOutlet weak var textEntryPlaceholderLabel: UILabel!
  @IBOutlet weak var dismissKeyboardButton: UIButton!
  @IBOutlet weak var topBorder: UIView! // TODO: Rename top border hairline
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var avatarContainerView: UIView!
  
  var avatarImage: UIImage? {
    didSet {
      avatarImageView?.image = avatarImage
    }
  }

  var avatarImageURL: NSURL? {
    didSet {
      setAvatarImageWithURLIfNeeded()
    }
  }
  
  var feedbackText: String {
    get {
      return feedbackTextView.text
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    appyStyle()
    applyInitialConfigurationForTextView()
    if let avatarImage = avatarImage {
      avatarImageView.image = avatarImage
    }
    setAvatarImageWithURLIfNeeded()
    avatarImageView.alpha = 0
  }

  func setAvatarImageWithURLIfNeeded() {
    guard let avatarImageURL = avatarImageURL else { return }
    let placeholder = UIImage(named: "FeedbackAvi")
    avatarImageView?.sd_setImageWithURL(avatarImageURL, placeholderImage: placeholder, options: .HandleCookies)
  }
  
  func appyStyle() {
    feedbackTextView.textColor = ADGTextColor
    textEntryPlaceholderLabel.textColor = ADGTextColorSecondary
    topBorder.backgroundColor = ADGHairlineColor
    avatarContainerView.layer.cornerRadius = 20
  }
  
  func applyInitialConfigurationForTextView() {
    dispatch_async(dispatch_get_main_queue()) {
      self.feedbackTextView.text = "" // Storyboard sets a random text value so that the text view will update its baseline.
    }
    feedbackTextView.delegate = self
    feedbackTextView.textContainerInset.top = 21
    textEntryPlaceholderLabel.hidden = false
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    adjustScrollEnabledAccordingToVerticalSpaceAvailable()
    adjustRightInsetToMakeRoomforDismissKeyboardButton()
  }
  
  func adjustScrollEnabledAccordingToVerticalSpaceAvailable() {
    // This logic depends on feedback view controller's extendend edges off for the top bars.
    if frame.origin.y <= 0 && !feedbackTextView.scrollEnabled {
      feedbackTextView.scrollEnabled = true
    } else if frame.origin.y > 0 && feedbackTextView.scrollEnabled {
      dispatch_async(dispatch_get_main_queue()) { // Apply on the next turn of the runloop.
        self.feedbackTextView.scrollEnabled = false
        self.feedbackTextView.invalidateIntrinsicContentSize()
      }
    }
  }
  
  func adjustRightInsetToMakeRoomforDismissKeyboardButton() {
    let dismissKeyboardToFeedbackTextViewhorizontalOverlap = feedbackTextView.frame.maxX - dismissKeyboardButton.frame.origin.x
    if dismissKeyboardToFeedbackTextViewhorizontalOverlap > 0 {
      feedbackTextView.textContainerInset.right = feedbackTextView.frame.maxX - dismissKeyboardButton.frame.origin.x
    } else {
      feedbackTextView.textContainerInset.right = 2
    }
  }
  
  func presentKeyboard() {
    feedbackTextView.becomeFirstResponder()
  }
  
  @IBAction func dismissKeyboard(sender: AnyObject) {
    dismissKeyboard()
  }
  
  func dismissKeyboard() {
    feedbackTextView.resignFirstResponder()
  }
  
}

extension ComposeFeedbackView: UITextViewDelegate {
  func textViewDidChange(textView: UITextView) {
    showOrHidePlaceholderLabelBasedOnEnteredText()
    adjustScrollEnabledAccordingToTextContentSizeHeight()
  }
  
  func showOrHidePlaceholderLabelBasedOnEnteredText() {
    if feedbackTextView.text.isEmpty {
      textEntryPlaceholderLabel.hidden = false
    } else {
      textEntryPlaceholderLabel.hidden = true
    }
  }
  
  func adjustScrollEnabledAccordingToTextContentSizeHeight() {
    if feedbackTextView.contentSize.height <= feedbackTextView.bounds.size.height && feedbackTextView.scrollEnabled  {
      feedbackTextView.scrollEnabled = false
      feedbackTextView.invalidateIntrinsicContentSize()
    }
  }
}
