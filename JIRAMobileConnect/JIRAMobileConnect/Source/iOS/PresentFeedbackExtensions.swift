//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit

private var feedbackNotOnScreen = true


// TODO: Add project sort script.
extension UIWindow {
  @objc
  public func presentFeedbackIfShakeMotion(motion: UIEventSubtype) {
    presentFeedbackIfShakeMotion(motion, promptUser: false, settings: try! FeedbackSettings())
  }
}


extension UIWindow {
  @objc // This method should only be used from ObjC
  public func presentFeedbackIfShakeMotion(motion: UIEventSubtype, promptUser: Bool = false, issueType: String? = nil,
      issueComponents: [String]? = nil, reporterAvatarImage: CGImage? = nil, reporterUsernameOrEmail: String? = nil) {
    guard let settings = try? FeedbackSettings(issueType: issueType, issueComponents: issueComponents, reporterAvatarImage: reporterAvatarImage, reporterUsernameOrEmail: reporterUsernameOrEmail) else {
      return
    }
    presentFeedbackIfShakeMotion(motion, promptUser: promptUser, settings: settings)
  }
  
  public func presentFeedbackIfShakeMotion(motion: UIEventSubtype, promptUser: Bool = false, settings: FeedbackSettings? = nil) {
    guard motion == .MotionShake else { return }
    guard feedbackNotOnScreen else { return }
    guard let presentedViewController = rootViewController?.topmostPresentedViewController else {
      print("\(self), Cannot present feedback prompt because window does not have a presented view controller.".formattedLoggingStatement)
      return
    }
    if promptUser {
      presentedViewController.presentFeedbackWithPromptAndScreenshotTransition(settings: settings)
    } else {
      presentedViewController.presentNewFeedbackFlowWithScreenshotTransition(settings: settings)
    }
  }
  
  public func presentFeedback(promptUser promptUser: Bool = false, settings: FeedbackSettings? = nil) {
    guard let presentedViewController = rootViewController?.topmostPresentedViewController else {
      print("\(self), Cannot present feedback prompt because window does not have a presented view controller.".formattedLoggingStatement)
      return
    }
    guard feedbackNotOnScreen else { return }
    presentedViewController.presentFeedback(promptUser: promptUser, settings: settings)
  }
}

extension UIViewController {
  func presentFeedbackWithPromptAndScreenshotTransition(settings settings: FeedbackSettings? = nil) {
    guard feedbackNotOnScreen else { return }
    self.topmostPresentedViewController.presentViewController(ViewControllerFactory().createPropmtController(useCustomTransition: true, settings: settings), animated: true, completion: nil)
  }
  
  func presentNewFeedbackFlowWithScreenshotTransition(settings settings: FeedbackSettings? = nil) {
    guard let window = view.window else {
      return
    }
    guard feedbackNotOnScreen else { return }
    feedbackNotOnScreen = false
    ViewControllerFactory().createNewFeedbackFlowControllerForScreenshotView(window, settings: settings, onFlowDidFinish: {
      dispatch_async(dispatch_get_main_queue()) { // TODO: Verify this gets called from main queue, if so remove dispatch_async.
        feedbackNotOnScreen = true
      }
    }) { flowController in
      self.topmostPresentedViewController.presentViewController(flowController, animated: true, completion: nil)
    }
  }
  
  @objc // This method should only be used from ObjC
  public func presentFeedback(promptUser promptUser: Bool = false, issueType: String? = nil, issueComponents: [String]? = nil,
      reporterAvatarImage: CGImage? = nil, reporterUsernameOrEmail: String? = nil) {
    guard let settings = try? FeedbackSettings(issueType: issueType, issueComponents: issueComponents, reporterAvatarImage: reporterAvatarImage, reporterUsernameOrEmail: reporterUsernameOrEmail) else {
      return
    }
    guard feedbackNotOnScreen else { return }
    presentFeedback(promptUser: promptUser, settings: settings)
  }
  
  
  public func presentFeedback(promptUser promptUser: Bool = false, settings: FeedbackSettings? = nil) {
    guard feedbackNotOnScreen else { return }
    if promptUser {
      self.topmostPresentedViewController.presentViewController(ViewControllerFactory().createPropmtController(useCustomTransition: false, settings: settings), animated: true, completion: nil)
    } else {
      self.presentNewFeedbackFlow(settings: settings)
    }
  }
  
  func presentNewFeedbackFlow(settings settings: FeedbackSettings? = nil) {
    guard feedbackNotOnScreen else { return }
    feedbackNotOnScreen = false
    ViewControllerFactory().createNewFeedbackFlowController(settings: settings, onFlowDidFinish: {
      dispatch_async(dispatch_get_main_queue()) { // TODO: Verify this gets called from main queue, if so remove dispatch_async.
        feedbackNotOnScreen = true
      }
    }) { flowController in
      self.topmostPresentedViewController.presentViewController(flowController, animated: true) {
        flowController.presentKeyboard()
      }
    }
  }
}

extension AlertController {
  func addNewFeedbackFlowAction() {
    self.addDefaultAction("entryPrompt_newFeedback_button_title") { [weak self] action in
      if let strongSelf = self {
        if strongSelf.useCustomTransition {
          strongSelf.presentNewFeedbackFlowWithScreenshotTransition_1()
        } else {
          strongSelf.presentNewFeedbackFlow_1()
        }
      }
    }
  }
  
  func presentNewFeedbackFlowWithScreenshotTransition_1() { // TODO: Alert controller is a bit messy now, find a better way to transport settings to feedback vc.
    guard let presentingViewController = self.lastPresentingViewController else { return }
    presentingViewController.presentNewFeedbackFlowWithScreenshotTransition(settings: self.feedbackSettings)
  }
  
  func presentNewFeedbackFlow_1() { // TODO: Remove default reporterIdentifier value from all methods.
    guard let presentingViewController = self.lastPresentingViewController else { return }
    presentingViewController.presentNewFeedbackFlow(settings: self.feedbackSettings)
  }
}

extension UIWindow {
  var topmostPresentedViewController: UIViewController? {
    return rootViewController?.topmostPresentedViewController
  }
}

extension UIViewController {
  var topmostPresentedViewController: UIViewController {
    var topMostViewController = self
    if let vc = _topmostPresentedViewController() {
      topMostViewController = vc
    }
    return topMostViewController
  }
  
  private func _topmostPresentedViewController() -> UIViewController? {
    if let presentedViewController = presentedViewController {
      if let topmostViewController = presentedViewController._topmostPresentedViewController() {
        return topmostViewController
      } else {
        return presentedViewController
      }
    } else {
      return nil
    }
  }
}



