//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit

class ViewControllerFactory {
  
  func createPropmtController(useCustomTransition useCustomTransition: Bool, settings: FeedbackSettings? = nil) -> UIViewController {
    let alertController: AlertController
    if UIScreen.mainScreen().traitCollection.horizontalSizeClass == .Regular {
      alertController = AlertController.createAlertController("entryPrompt_alert_title", messageKey: "entryPrompt_alert_message")
    } else {
      alertController = AlertController.createAlertSheetController("entryPrompt_alert_title", messageKey: "entryPrompt_alert_message")
    }
    alertController.feedbackSettings = settings // TODO: Make this assignment part of initialization, current temporal coupling.
    alertController.addNewFeedbackFlowAction()
    alertController.addCancelAction()
    alertController.useCustomTransition = useCustomTransition
    
    
    
    return alertController
  }
  
  func createNewFeedbackFlowControllerForScreenshotView(viewForScreenshot: UIView, settings: FeedbackSettings? = nil, onFlowDidFinish: (Void->Void)? = nil, onComplete: (NewFeedbackFlowController)->())  {
    viewForScreenshot.takeScreenshotWithFlashAnimation() { screenshot in
      let flowController = NewFeedbackFlowController(screenshot: screenshot, settings: settings)
      flowController.onDidFinish = onFlowDidFinish
      onComplete(flowController)
    }
  }
  
  func createNewFeedbackFlowController(settings settings: FeedbackSettings? = nil, onFlowDidFinish: (Void->Void)? = nil, onComplete: (NewFeedbackFlowController)->()) {
    let flowController = NewFeedbackFlowController(screenshot: nil, settings: settings)
    flowController.shouldUseCustomTransition = false
    flowController.onDidFinish = onFlowDidFinish
    onComplete(flowController)
  }
}

extension String {
  var localizedString: String {
    get {
      let bundle = Bundle.JIRAMobileConnectBundle
      return NSLocalizedString(self, bundle: bundle, comment: "")
    }
  }
}
