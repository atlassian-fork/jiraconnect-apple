//  Copyright (c) 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

public class AsyncAction: NSOperation {
  
  private var _executing = false
  private var _finished = false
  
  override private(set) public var executing: Bool {
    get {
      return _executing
    }
    set {
      willChangeValueForKey("isExecuting")
      _executing = newValue
      didChangeValueForKey("isExecuting")
    }
  }
  
  override private(set) public var finished: Bool {
    get {
      return _finished
    }
    set {
      willChangeValueForKey("isFinished")
      _finished = newValue
      didChangeValueForKey("isFinished")
    }
  }
  
  override public var completionBlock: (() -> Void)? {
    set {
      super.completionBlock = newValue
    }
    get {
      return {
        super.completionBlock?()
        self.actionCompleted()
      }
    }
  }
  
  override public var asynchronous: Bool {
    return true
  }
  
  override public func start() {
    if cancelled {
      finished = true
      return
    }
    
    executing = true
    autoreleasepool {
      self.run()
    }
  }
  
  func run() {
    preconditionFailure("This abstract method must be overridden.")
  }
  
  func actionCompleted() {
    //optional
  }
  
  func finishedExecutingOperation() {
    executing = false
    finished = true
  }
}
