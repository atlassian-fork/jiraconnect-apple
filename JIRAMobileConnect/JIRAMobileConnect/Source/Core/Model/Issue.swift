//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

public struct Issue {
  let summary: String
  let description: String
  let components: [String]
  let type: String
  
  public init(summary: String, description: String, components: [String], type: String, reporterUsernameOrEmail: String? = nil) {
    self.summary = summary
    self.description = description.withReporterUsernameOrEmailAppended(reporterUsernameOrEmail: reporterUsernameOrEmail)
    self.components = components
    self.type = type
  }
  
  public init(feedback: String, components: [String], type: String, reporterUsernameOrEmail: String? = nil) {
    switch feedback.characters.count {
    case let count where count > 80:
      // TODO: Add ellipses if description is more than 80 chars.
      let frontRange: Range<String.Index> = feedback.startIndex...feedback.startIndex.advancedBy(80)
      self.init(summary: feedback.substringWithRange(frontRange), description: feedback, components: components, type: type, reporterUsernameOrEmail: reporterUsernameOrEmail)
    default:
      self.init(summary: feedback, description: feedback, components: components, type: type, reporterUsernameOrEmail: reporterUsernameOrEmail)
    }
  }
}

extension String {
  private func withReporterUsernameOrEmailAppended(reporterUsernameOrEmail reporterUsernameOrEmail: String?) -> String {
    guard let reporterUsernameOrEmail = reporterUsernameOrEmail else { return self }
    return "\(self) \n\n Submitted by: \(reporterUsernameOrEmail)" // TODO: Use localized string.
  }
}
