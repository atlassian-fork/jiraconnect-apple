//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

extension JMCTarget {
  static let JSONFileName = "JMCTarget"
  
  public static func createTargetFromJSONOnDisk() throws -> JMCTarget {
    guard let JMCTargetJSONPath = NSBundle.mainBundle().pathForResource(JSONFileName, ofType: "json") else {
      throw JMCTargetJSONOnDiskError.JSONFileMissingFromBundleError
    }
    return try JMCTarget(JSONFilePath: JMCTargetJSONPath)
  }
  
  init(JSONFilePath: String) throws {
    guard let JMCTargetData = NSData(contentsOfFile: JSONFilePath) else {
      throw JMCTargetJSONOnDiskError.ReadJSONFileError
    }
    try self.init(JSONData: JMCTargetData)
  }
  
  init(JSONData: NSData) throws {
    let JSONObject = try NSJSONSerialization.JSONObjectWithData(JSONData, options: [])
    guard let JMCTargetDictionary = JSONObject  as? [String : NSObject] else {
      throw JMCTargetJSONOnDiskError.JSONDictionaryToInstanceError
    }
    try self.init(JSONDictionary: JMCTargetDictionary )
  }
  
  init(JSONDictionary: [String: NSObject]) throws {
    guard let host = JSONDictionary["host"] as? String,
      apiKey = JSONDictionary["apiKey"] as? String,
      projectKey = JSONDictionary["projectKey"] as? String else {
        throw JMCTargetJSONOnDiskError.JSONDictionaryToInstanceError
    }
    self.init(host: host, apiKey: apiKey, projectKey: projectKey)
  }
  
}

public enum JMCTargetJSONOnDiskError: ErrorType {
  case JSONFileMissingFromBundleError
  case ReadJSONFileError
  case JSONDictionaryToInstanceError
}
