//: Playground - noun: a place where people can play

import XCPlayground

XCPlaygroundPage.currentPage.needsIndefiniteExecution = true

import UIKit
import JIRAMobileConnectForPlayground
import JIRAMobileConnect


let screenshotImage = UIImage(named: "screenshot")

let target = JMCTarget(host: "rcachatx.atlassian.net", apiKey: "af1f4aa5-46d9-4da2-a238-e6f2affdace4", projectKey: "HCVQ1")
let issue = Issue(summary: "Test", description: "Test", components: ["iOS"], type: "Bug")

let action = SendFeedbackAction(issue: issue, screenshotImageOrNil: screenshotImage) { outcome in
    switch outcome {
    case .Success:
        print("Success")
    case .Error:
        print("Error")
    case .Cancelled:
        print("Cancelled")
    }
    
}

//action.start()

let targetFromDisk = try JMCTarget.createTargetFromJSONOnDisk()
